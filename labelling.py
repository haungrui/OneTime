import openpyxl

#list3，list4是子表对应的需要贴标签的数字。上下一一对应。
list3 = [1,1,2,2,3,3,4,4,5,5,6,6]
list4 = [8,10,7,12,9,11,7,10,8,12,9,11]
#默认贴的数字
defaultdata = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
#data里面贴标签区间的行数
start=250
end=3130

#存储要贴标签的数
data = []

# 判断属于哪个区间
def judge(data):
    for i in range(0,int((end-start)/20)):
        if ((i*20+start)<=data) & (data<((i+1)*20+start)):
            return i*20+start,(i+1)*20+start

#测试循环添加需要贴标签的数字到列表
def addlable(lable):
    for i in range(0,20):
        data.append(lable)
    return data

#定义一个打开表的方法，因为要写的比较多，抽象出来比较好
def openExcel(lable,key):
    #创建两个列表存储event子表中表名的信息
    list1 = ['01','02','03','04','05','06','07','08','09','10','11','12']
    list2 = ['B','D','G','L','O','Q','S','V','Z','4','7','9',]
    #打开用于查找区间表进行操作(event)
    findPath = 'S'+str(lable)+'_train_event.xlsx'
    findwb = openpyxl.load_workbook(findPath)
    findwritesheet = 'char'+list1[key]+'('+list2[key]+')'
    findsh = findwb[findwritesheet]  #这里的是用来查找的表
    #打开用于贴标签区间表进行操作(data)
    writePath = 'S'+str(lable)+'_train_data.xlsx'
    print(type(writePath))
    return findPath,writePath,findwritesheet

def execute():
    for lable in range(1,6): #1-6
        for key in range (0,12): #0-12
            findPath = openExcel(lable,key)[0]
            writePath = openExcel(lable,key)[1]
            findwritesheet = openExcel(lable,key)[2]
            findSection(findPath,writePath,key,findwritesheet)

#先从event表中找出相应的区间,找到之后调用写的方法
#lable :当前打开的表名,这里只传表的数字即可
#key:要找的贴标签的数字在list3和list4中的位置
def findSection(findPath,writePath,key,findwritesheet):
    #存储要贴0的标签
    needsection = [start]
    findwb = openpyxl.load_workbook(findPath)
    findsh = findwb[findwritesheet]
    writewb=openpyxl.load_workbook(writePath)
    for cases in list(findsh.rows)[0:]:
        case_id =  cases[0].value
        case_excepted = cases[1].value
        if case_id==list3[key]:
            area = judge(case_excepted)
            #需要写进data表的标签的数字
            data = addlable(list3[key])
            print(data,writePath,area[0],area[1],findwritesheet,list3[key])
            needsection.append(area[0])
            needsection.append(area[1])
            WriteExcel(data,writewb,area[0],area[1],findwritesheet)
            data.clear()
        if case_id==list4[key]:
            area = judge(case_excepted)
            #需要写进data表的标签的数字
            data = addlable(list4[key])
            print(data,writePath,area[0],area[1],findwritesheet,list4[key])
            needsection.append(area[0])
            needsection.append(area[1])
            WriteExcel(data,writewb,area[0],area[1],findwritesheet)
            data.clear()
    needsection.append(end)
    # sorted(needsection)
    i=0
    print('------------------------------------------')
    print(needsection)
    print(len(needsection))
    print('------------------------------------------')
    while (i<(len(needsection)-1)):
        print(i)
        print(needsection[i],needsection[i+1])
        print(defaultdata,writePath,area[0],area[1],findwritesheet,list4[key],needsection)
        WriteExcel(defaultdata,writewb,needsection[i],needsection[i+1],findwritesheet)
        i=i+2
    needsection.clear()
    findwb.close()
    writewb.save(writePath)

#定义一个对excel表进行操作的函数
def WriteExcel(data,writewb,rangeA,rangeB,findwritesheet):
    # wb=openpyxl.load_workbook(writePath)
    writesh = writewb[findwritesheet]
    for i in range(rangeA,rangeB):
        for j in range(1,len(data)+1):
            distance=data[j-1]
            writesh.cell(row = i, column = j).value = distance


#测试向excel中写数据
# data = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
# path = r'S1_train_data.xlsx'
# WriteExcel(data,path,30,50+1)


# 测试判断区间
# k = judge(355)
# print(k[0],k[1])

#总执行函数
execute()
